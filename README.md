# Trovo Stream Deck Plugin

A generic plugin for Stream Deck that allows you to see how many viewers you currently have, your total follower count, and total subscriber count. There will be more features but I wanted to start simple first.